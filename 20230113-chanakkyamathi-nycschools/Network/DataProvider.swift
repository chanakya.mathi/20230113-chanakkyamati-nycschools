//
//  DataProviders.swift
//  20230113-chanakkyamathi-nycschools
//
//  Created by Chanakya Mathi on 1/13/23.
//

import Foundation

// TODO: The Data Providers can return JSON Models and We can have another Layer which Transforms the JSON Models into DOMAIN models, for now the data providers are directly returning JSON Models

protocol SchoolSATDataProviderType {
    func getHighSchoolSatScore(dbn: String, success: @escaping (SchoolSATScore) -> Void, failure: @escaping (Error) -> Void)
}

protocol NYCHighSchoolDataProviderType {
    func getNycHighSchoolDirectory(success: @escaping ([School]) -> Void, failure: @escaping (Error) -> Void)
}

class NYCHighSchoolDataProvider: NYCHighSchoolDataProviderType {

    struct API {
        static let getNycHighSchoolDirectory =
        URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
    }

    private let httpRequestHandler: HttpRequestHandlerType

    init(httpRequestHandler: HttpRequestHandlerType? = nil) {
        self.httpRequestHandler = httpRequestHandler ?? HttpRequestHandler()
    }

    func getNycHighSchoolDirectory(success: @escaping ([School]) -> Void, failure: @escaping (Error) -> Void) {
        guard let url = API.getNycHighSchoolDirectory else {
            // handle by logging or failing or showing an error
            preconditionFailure("Url can't be nil")
        }

        let request = URLRequest(url: url)
        httpRequestHandler.jsonRequest(request) { (result: Result<[School], Error>) in
            switch result {
            case .success(let highSchoolDirectory):
                DispatchQueue.main.async {
                    success(highSchoolDirectory)
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    failure(error)
                }
            }
        }
    }

}

class NYCHighSchoolSATDataProvider: SchoolSATDataProviderType {

    struct API {
        enum ServiceError: Error {
            case noResults
        }
        static let getNycHighSchooSATDataEndPoint = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }

    private let httpRequestHandler: HttpRequestHandlerType

    init(httpRequestHandler: HttpRequestHandlerType = HttpRequestHandler()) {
        self.httpRequestHandler = httpRequestHandler
    }

    func getHighSchoolSatScore(dbn: String, success: @escaping (SchoolSATScore) -> Void, failure: @escaping (Error) -> Void) {
        var urlComponents = URLComponents(string: API.getNycHighSchooSATDataEndPoint)
        urlComponents?.queryItems = [ URLQueryItem(name: "dbn", value: dbn) ]

        guard let url = urlComponents?.url else {
            // handle by logging or failing or showing an error
            preconditionFailure("Url can't be nil")
        }

        let request = URLRequest(url: url)
        httpRequestHandler.jsonRequest(request) { (result: Result<[SchoolSATScore], Error>) in
            switch result {
            case .success(let filterResults):
                guard let satScore = filterResults.first else {
                    DispatchQueue.main.async {
                        failure(API.ServiceError.noResults)
                    }
                    return
                }

                DispatchQueue.main.async {
                    success(satScore)
                }

            case .failure(let error):
                DispatchQueue.main.async {
                    failure(error)
                }
            }
        }

    }
}
