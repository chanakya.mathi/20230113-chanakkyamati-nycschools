//
//  HttpRequestHandler.swift
//  20230113-chanakkyamathi-nycschools
//
//  Created by Chanakya Mathi on 1/13/23.
//

import Foundation

protocol HttpRequestHandlerType {
    func jsonRequest<ResponseEntity: Codable>(_ request: URLRequest, completionHandler: @escaping (Result<ResponseEntity, Error>) -> Void)
}

class HttpRequestHandler:  HttpRequestHandlerType {

    private let session: URLSession = {
        let configuration: URLSessionConfiguration  = {
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 60.0
            return configuration
        }()

        return URLSession(configuration: configuration)
    }()

    func jsonRequest<ResponseEntity: Codable>(_ request: URLRequest, completionHandler: @escaping (Result<ResponseEntity, Error>) -> Void) {

        let dataTask = session.dataTask(with: request) { (data, urlResponse, error) in
            if let error = error {
                completionHandler(.failure(error))
                return
            }

            guard let responseData = data,
                  let httpUrlResponse = urlResponse as? HTTPURLResponse,
                  (200..<300).contains(httpUrlResponse.statusCode) else {
                completionHandler(.failure(NetworkError.serverError(urlResponse, data)))
                return
            }

            do {
                let jsonResponse =  try JSONDecoder().decode(ResponseEntity.self, from: responseData)
                completionHandler(.success(jsonResponse))
            } catch  let decodingError {
                completionHandler(.failure(decodingError))
            }
        }

        dataTask.resume()
    }

    enum NetworkError: Error {
        case serverError(URLResponse?, Data?)
    }
}
