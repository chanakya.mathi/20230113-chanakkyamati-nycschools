//
//  NycHighSchoolDirectoryViewModel.swift
//  20230113-chanakkyamathi-nycschools
//
//  Created by Chanakya Mathi on 1/13/23.
//

import Foundation

protocol NycHighSchoolDirectoryViewModelType: AnyObject {
    var title: String { get }
    var numberOfSchools: Int { get }
    var shouldShowLoader: Bool { get }
    var OnFeedUpdate: (() -> ())? { get set }
    var onRefreshStateUpdate: (() -> ())? { get set }
    var showSchoolDetail:((SchoolDetailViewModel) -> ())? { get set }
    func schoolCellViewModel(at indexPath: IndexPath) -> SchoolCellViewModelType
    func fetchSchools(success: @escaping ([School]) -> Void, failure: @escaping (Error) -> Void)
    func didSelectSchool(at indexPath: IndexPath)

}

class NycHighSchoolDirectoryViewModel: NycHighSchoolDirectoryViewModelType {

    private let dataProvider: NYCHighSchoolDataProviderType

    private(set) var schools: [School] = [] {
        didSet {
            OnFeedUpdate?()
        }
    }
    private var fetchStatus: FetchStatus = .none {
        didSet {
            onRefreshStateUpdate?()
        }
    }
    var OnFeedUpdate: (() -> ())?
    var onRefreshStateUpdate: (() -> ())?
    var showSchoolDetail: ((SchoolDetailViewModel) -> ())?
    var title: String {
        NSLocalizedString("NYC High Schools", comment: "Directory Title")
    }

    init(dataProvider: NYCHighSchoolDataProviderType = NYCHighSchoolDataProvider()) {
        self.dataProvider = dataProvider
    }

    func fetchSchools(success: @escaping ([School]) -> Void, failure: @escaping (Error) -> Void) {
        fetchStatus = .isLoading
        dataProvider.getNycHighSchoolDirectory { [weak self] directory in
            self?.schools = directory
            self?.fetchStatus = .success
            success(directory)
        } failure: { [weak self] error in
            self?.fetchStatus = .failure
            failure(error)
        }
    }

    func didSelectSchool(at indexPath: IndexPath) {
        showSchoolDetail?(SchoolDetailViewModel(school: schools[indexPath.row]))
    }
}

extension NycHighSchoolDirectoryViewModel {
    var shouldShowLoader: Bool {
        fetchStatus == .isLoading
    }

    var shouldShowError: Bool {
        fetchStatus == .failure
    }

    var numberOfSchools: Int {
        schools.count
    }

    func schoolCellViewModel(at indexPath: IndexPath) -> SchoolCellViewModelType {
        SchoolCellViewModel(school: schools[indexPath.row])
    }
}

enum FetchStatus: Equatable {
    case isLoading
    case none
    case success
    case failure
}


