//
//  SchoolDetailViewModel.swift
//  20230113-chanakkyamathi-nycschools
//
//  Created by Chanakya Mathi on 1/13/23.
//

import Foundation
import Combine

protocol SchoolDetailViewModelDelegate: AnyObject {
    func didUpdateFetchStatus(viewModel: SchoolDetailViewModel)
    func didUpdateContent(viewModel: SchoolDetailViewModel)
}


class SchoolDetailViewModel {
    private let school: School
    private let schoolSATDataProvider: SchoolSATDataProviderType
    weak var delegate: SchoolDetailViewModelDelegate?
    private var satScore: SchoolSATScore? {
        didSet {
            delegate?.didUpdateContent(viewModel: self)
        }
    }

    private var fetchStatus: FetchStatus = .none {
        didSet {
            delegate?.didUpdateFetchStatus(viewModel: self)
        }
    }

    init(
        school: School,
        schoolSATDataProvider: SchoolSATDataProviderType = NYCHighSchoolSATDataProvider()
    ){
        self.school = school
        self.schoolSATDataProvider = schoolSATDataProvider
    }

    func getSatScore(success: @escaping (SchoolSATScore) -> Void, failure: @escaping (Error) -> Void) {
        fetchStatus = .isLoading
        schoolSATDataProvider.getHighSchoolSatScore(dbn: school.dbn) { [weak self] satScore in
            self?.satScore = satScore
            self?.fetchStatus = .success
            success(satScore)
        } failure: { [weak self] error in
            self?.fetchStatus = .failure
            failure(error)
        }
    }
}

extension SchoolDetailViewModel {
    var schoolName: String {
        school.school_name
    }

    var numberOfTestTakers: String? {
        satScore?.num_of_sat_test_takers.map {
            "Number Of Test Takers: \($0)"
        }
    }

    var averageReadingScore: String? {
        satScore?.sat_critical_reading_avg_score.map {
            "Average Reading Score: \($0)"
        }
    }

    var averageWritingScore: String? {
        satScore?.sat_writing_avg_score.map {
            "Average Writing Score: \($0)"
        }
    }

    var averageMathScore: String? {
        satScore?.sat_math_avg_score.map {
            "Average Math Score: \($0)"
        }
    }

    var shouldShowLoader: Bool {
        fetchStatus == .isLoading
    }

    var shouldShowError: Bool {
        fetchStatus == .failure
    }
}
