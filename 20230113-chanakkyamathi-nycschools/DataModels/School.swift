//
//  School.swift
//  20230113-chanakkyamathi-nycschools
//
//  Created by Chanakya Mathi on 1/13/23.
//

import Foundation

struct School: Codable, Equatable {
    let dbn: String
    let school_name: String
    let boro: String?
    let overview_paragraph: String?
    let location: String?
    let phone_number: String?
}

struct SchoolSATScore: Codable {
    let dbn: String
    let school_name: String
    let num_of_sat_test_takers: String?
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?
}

