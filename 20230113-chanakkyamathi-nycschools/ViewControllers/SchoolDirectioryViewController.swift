//
//  SchoolDirectioryViewController.swift
//  20230113-chanakkyamathi-nycschools
//
//  Created by Chanakya Mathi on 1/13/23.
//

import UIKit

class SchoolDirectioryViewController: UITableViewController {

    let viewModel: NycHighSchoolDirectoryViewModelType
    private lazy var activityIndicator = UIActivityIndicatorView(style: .large)

    // MARK: - Init
    init(viewModel: NycHighSchoolDirectoryViewModelType = NycHighSchoolDirectoryViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupCellRegistarion()
        viewModelRegistrations()
        refresh()
    }

    private func viewModelRegistrations() {
        viewModel.OnFeedUpdate = { [weak self] in
            self?.tableView.reloadData()
        }

        viewModel.onRefreshStateUpdate = {  [weak self] in
            guard let self = self else {
                return
            }
            self.viewModel.shouldShowLoader ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = !self.viewModel.shouldShowLoader
        }

        viewModel.showSchoolDetail = { [weak self] detailViewModel in
            let detailViewController = SchoolDetailViewController(viewModel: detailViewModel)
            self?.navigationController?.pushViewController(detailViewController, animated: true)
        }
    }

    private func setupView() {
        title = viewModel.title
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsMultipleSelection = false
        clearsSelectionOnViewWillAppear = true
        view.addSubview(activityIndicator)
        activityIndicator.pinToSuperviewCenter(offsetX: 0, offsetY: 0)
    }

    private func setupCellRegistarion() {
        tableView.register(SchoolTableViewCell.self, forCellReuseIdentifier: "SchoolTableViewCell")
    }

    func refresh() {
        viewModel.fetchSchools(success: { _ in }, failure: {_ in })
    }
}

extension SchoolDirectioryViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfSchools
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolTableViewCell", for: indexPath) as? SchoolTableViewCell else {
            preconditionFailure("Programming error, cell should be of type SchoolTableViewCell")
        }
        cell.configure(with: viewModel.schoolCellViewModel(at: indexPath))
        cell.accessoryType = .disclosureIndicator
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectSchool(at: indexPath)
    }
}
