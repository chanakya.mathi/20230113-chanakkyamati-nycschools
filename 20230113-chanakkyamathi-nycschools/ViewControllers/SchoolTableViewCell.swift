//
//  AlbumTableViewCell.swift
//  20230113-chanakkyamathi-nycschools
//
//  Created by Chanakya Mathi on 1/13/23.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    private let labelContainerStack: UIStackView = {
        let stack = UIStackView()
        stack.isLayoutMarginsRelativeArrangement = true
        stack.layoutMargins = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        stack.axis = .vertical
        stack.spacing = 10.0
        return stack
    }()

    let schoolNameLabel = UILabel.multiLineLabel()

    var viewModel: SchoolCellViewModelType?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        viewModel = nil
    }

    private func setupView() {
        contentView.addSubview(labelContainerStack)
        labelContainerStack.pinToSuperView()
        labelContainerStack.addArrangedSubviews(schoolNameLabel)
    }

}

extension SchoolTableViewCell {
    func configure(with viewModel: SchoolCellViewModelType) {
        self.viewModel = viewModel
        schoolNameLabel.text = viewModel.name
    }
}

// ViewModel
protocol SchoolCellViewModelType {
    var name: String { get }
    var location: String? { get }
}

struct SchoolCellViewModel: SchoolCellViewModelType {
    let school: School

    var name: String {
        school.school_name
    }

    var location: String? {
        school.location
    }
}


