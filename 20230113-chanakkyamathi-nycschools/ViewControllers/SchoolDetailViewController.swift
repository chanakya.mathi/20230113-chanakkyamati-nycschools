//
//  SchoolDetailViewController.swift
//  20230113-chanakkyamathi-nycschools
//
//  Created by Chanakya Mathi on 1/13/23.
//

import Foundation
import UIKit

// TODO: Need to Add AN Error UI, to show Error when networkcall fails
class SchoolDetailViewController: UIViewController {
    lazy private(set) var stack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 10.0
        stack.alignment = .top
        stack.distribution = .fill
        stack.isLayoutMarginsRelativeArrangement = true
        stack.layoutMargins = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        return stack
    }()

    lazy private(set) var satTakersCountLabel = UILabel()
    lazy private(set) var avgMathScoreLabel = UILabel()
    lazy private(set) var avgReadingScoreLabel = UILabel()
    lazy private(set) var avgWritingScoreLabel = UILabel()
    private lazy var activityIndicator = UIActivityIndicatorView(style: .large)

    let viewModel: SchoolDetailViewModel

    init(viewModel: SchoolDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        viewModel.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        fetchData()
    }

    func setupView() {
        title = viewModel.schoolName
        view.backgroundColor = UIColor.white
        view.addSubview(stack)
        view.addSubview(activityIndicator)
        activityIndicator.pinToSuperviewCenter(offsetX: 0, offsetY: 0)
        stack.pinToSafeArea(leading: 0, top: 0, trailing: 0)
        stack.addArrangedSubviews(
            satTakersCountLabel,
            avgMathScoreLabel,
            avgReadingScoreLabel,
            avgWritingScoreLabel,
            UIView()
        )
    }

    func fetchData() {
        viewModel.getSatScore(success: { _ in }, failure: { _ in })
    }
}

extension SchoolDetailViewController: SchoolDetailViewModelDelegate {
    func didUpdateFetchStatus(viewModel: SchoolDetailViewModel) {
        viewModel.shouldShowLoader ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
        activityIndicator.isHidden = !self.viewModel.shouldShowLoader
        stack.isHidden = self.viewModel.shouldShowLoader
    }

    func didUpdateContent(viewModel: SchoolDetailViewModel) {
        satTakersCountLabel.text = viewModel.numberOfTestTakers
        avgMathScoreLabel.text = viewModel.averageMathScore
        avgReadingScoreLabel.text = viewModel.averageReadingScore
        avgWritingScoreLabel.text = viewModel.averageWritingScore
    }
}
