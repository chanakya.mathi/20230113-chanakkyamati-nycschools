# 20230113 Chanakkyamathi Nycschools

The Project Consists of a TableViewController(SchoolDirectioryViewController) which shows the list of all schools
When Tapped on a School the detail page is pushed (SchoolDetailViewController)

List Of Major Components to Look at

SchoolDirectioryViewController
- NycHighSchoolDirectoryViewModel
- NYCHighSchoolDataProvider

SchoolDetailViewController
- SchoolDetailViewModel
- SchoolSATDataProviderType



