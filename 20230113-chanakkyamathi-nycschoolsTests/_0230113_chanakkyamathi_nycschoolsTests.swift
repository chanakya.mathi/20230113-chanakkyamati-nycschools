//
//  _0230113_chanakkyamathi_nycschoolsTests.swift
//  20230113-chanakkyamathi-nycschoolsTests
//
//  Created by Chanakya Mathi on 1/13/23.
//

import XCTest
@testable import _0230113_chanakkyamathi_nycschools

class NycHighSchoolDirectoryViewModelTests: XCTestCase {

    lazy var sut = NycHighSchoolDirectoryViewModel(dataProvider: mockNetworkDependency)
    var mockNetworkDependency = MockNycHighSchoolDataProvider()

    override func setUpWithError() throws {
        mockNetworkDependency = MockNycHighSchoolDataProvider()
        sut = NycHighSchoolDirectoryViewModel(dataProvider: mockNetworkDependency)
    }

    func test_AlbumsFeedViewModel() throws {
        // Given
        mockNetworkDependency.shouldSucceed = true
        mockNetworkDependency.isCalled = false

        let success_expectation = expectation(description: "should succeed")
        var refreshUpdateCallbackCalled = false
        var feedUpdateCallbackCalled = false
        sut.onRefreshStateUpdate = {
            refreshUpdateCallbackCalled = true
        }

        sut.OnFeedUpdate = {
            feedUpdateCallbackCalled = true
        }

        // When
        sut.fetchSchools { directory in
            success_expectation.fulfill()
        } failure: { _ in
            XCTFail("shouldn't fail")
            success_expectation.fulfill()
        }
        wait(for: [success_expectation], timeout: 1.0)

        // Then
        XCTAssertTrue(mockNetworkDependency.isCalled)
        XCTAssertEqual(sut.schools, School.mock)
        XCTAssertEqual(sut.numberOfSchools, 3)
        XCTAssertTrue(refreshUpdateCallbackCalled)
        XCTAssertTrue(feedUpdateCallbackCalled)
    }

}

extension School {
    static let mock: [School] = JsonLoad.getEntity(fromResource: "AlbumFeedResponse")
}

class MockNycHighSchoolDataProvider: NYCHighSchoolDataProviderType {
    var shouldSucceed = true
    var isCalled = false

    func getNycHighSchoolDirectory(success: @escaping ([School]) -> Void, failure: @escaping (Error) -> Void) {
        isCalled = true
        guard shouldSucceed else {
            failure(HttpRequestHandler.NetworkError.serverError(nil, nil))
            return
        }
        success(School.mock)
    }
}
